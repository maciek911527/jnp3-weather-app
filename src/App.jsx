import React from 'react';
import Map from './components/map/Map';
import Search from './components/search/Search';
import Loader from './components/loading/Loader';
import Charts from './components/charts/Charts';
import { AppWrapper } from './AppWraper';

function App() {
    return (
        <AppWrapper>
            <Search />
            <Charts />
            <Map />
            <Loader />
        </AppWrapper>
    );
}

export default App;
