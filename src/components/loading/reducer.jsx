import { createSlice } from "@reduxjs/toolkit";

export const LOADER_REDUCER_NAME = 'loader';

const loaderSlice = createSlice({
    name: LOADER_REDUCER_NAME,
    initialState: {
        loading: true
    },
    reducers: {
        setLoading: (state, action) => {
            state.loading = action.payload;
        },
    },
});

export const { setLoading } = loaderSlice.actions;

export default loaderSlice.reducer;