import React from 'react';
import styled from "styled-components";
import { Circles } from "react-loader-spinner";
import { useSelector } from 'react-redux';

const LoaderWrapper = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 1000;
`;

const Loader = () => {
    const loader = useSelector((state) => state.loader.loading);

    return (
        loader && (
            <LoaderWrapper>
                <Circles
                    height="80"
                    width="80"
                    radius="9"
                    color="grey"
                    ariaLabel="loading-indicator"
                />
            </LoaderWrapper>
        )
    );
}

export default Loader;
