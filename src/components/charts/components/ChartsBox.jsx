import styled from "styled-components";

export const ChartsBox = styled.div`
    position: fixed;
    top: 18vh;
    right: 1vw;
    width: 40vw;
    height: 83vh;
    background-color: ${props => props.colorMode === 'dark' ? '#2e2e2d' : '#5898bf'};
    z-index: 1005;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    overflow: scroll;
`;