import React from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import { useSelector } from 'react-redux';
import { ChartsBox } from './components/ChartsBox';
import { ChartWrapper } from './components/ChartWrapper';

const Charts = () => {
    const weatherData   = useSelector((state) => state.weather.weatherData);
    const colorMode     = useSelector((state) => state.search.colorMode)
    const chartsEnabled = useSelector((state) => state.search.chartsEnabled);

    const tempreatureChartData = Array.from(weatherData).map(([cityName, weatherDetails]) => ({
        name: cityName,
        temperature: weatherDetails.temperature,
    }));

    const pressureChartData = Array.from(weatherData).map(([cityName, weatherDetails]) => ({
        name: cityName,
        pressure: weatherDetails.pressure,
    }));

    const emojiChartData = Array.from(weatherData).map(([cityName, weatherDetails]) => ({
        name: cityName,
        emoji: weatherDetails.score,
    }));

    const renderEmojiTick = (value) => {
        switch (value) {
            case 3:
                return '😄';
            case 2:
                return '😐';
            case 1:
                return '🙁';
            default:
                return '';
        }
    };

    const chartColor = colorMode === 'dark' ? '#049104' : 'black';
    const tooltipColor = colorMode === 'white'

    function CustomTooltip({ payload, label, active }) {
        if (active && payload && payload.length) {
            return (
                <div className="custom-tooltip">
                    <p className="label" style={{ color: tooltipColor }}>{`${label} : ${payload[0].value}`}</p>
                </div>
            );
        }

        return null;
    }

    if (!chartsEnabled) return null;

    return (
        <ChartsBox colorMode={colorMode}>
            <ChartWrapper>
                <BarChart
                    width={550}
                    height={650}
                    data={tempreatureChartData}
                    margin={{
                        top: 350,
                        right: 30,
                        left: 20,
                        bottom: 70,
                    }}
                >
                    <CartesianGrid strokeDasharray="5 5" stroke={chartColor} />
                    <XAxis dataKey="name" angle={-45} textAnchor="end" stroke={chartColor} />
                    <YAxis type="number" domain={['dataMin - 1', 'dataMax + 1']} stroke={chartColor} />
                    <Tooltip content={<CustomTooltip />} />
                    <Legend
                        verticalAlign="top"
                        align='left'
                        wrapperStyle={{
                            paddingBottom: '20px'
                        }}
                    />
                    <Bar dataKey="temperature" fill={chartColor} name="Temperature" />
                </BarChart>
            </ChartWrapper>
            <ChartWrapper>
                <BarChart
                    width={550}
                    height={300}
                    data={pressureChartData}
                    margin={{
                        top: 10,
                        right: 30,
                        left: 20,
                        bottom: 70,
                    }}
                >
                    <CartesianGrid strokeDasharray="5 5" stroke={chartColor} />
                    <XAxis dataKey="name" angle={-45} textAnchor="end" stroke={chartColor} />
                    <YAxis type="number" domain={['dataMin - 10', 'dataMax + 10']} stroke={chartColor} />
                    <Tooltip content={<CustomTooltip />} />
                    <Legend
                        verticalAlign="top"
                        align='left'
                        wrapperStyle={{
                            paddingBottom: '20px'
                        }}
                    />
                    <Bar dataKey="pressure" fill={chartColor} name="Pressure" />
                </BarChart>
            </ChartWrapper>
            <ChartWrapper>
                <BarChart
                    width={550}
                    height={350}
                    data={emojiChartData}
                    margin={{
                        top: 30,
                        right: 30,
                        left: 20,
                        bottom: 70,
                    }}
                >
                    <CartesianGrid strokeDasharray="5 5" stroke={chartColor} />
                    <XAxis dataKey="name" angle={-45} textAnchor="end" stroke={chartColor} />
                    <YAxis tickFormatter={renderEmojiTick} stroke={chartColor} />
                    <Tooltip content={<CustomTooltip />} />
                    <Legend
                        verticalAlign="top"
                        align='left'
                        wrapperStyle={{
                            paddingBottom: '20px'
                        }}
                    />
                    <Bar dataKey="emoji" fill={chartColor} name="Emoji" />
                </BarChart>
            </ChartWrapper>
        </ChartsBox>
    );
}

export default Charts;
