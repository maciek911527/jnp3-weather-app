import { createAction, createSlice } from "@reduxjs/toolkit";

export const WEATHER_REDUCER_NAME = 'weather';

const weatherSlice = createSlice({
    name: WEATHER_REDUCER_NAME,
    initialState: {
        weatherData: new Map()
    },
    reducers: {
        setWeather: (state, action) => {
            state.weatherData = action.payload
        }
    },
});

export const fetchWeather = createAction(`${WEATHER_REDUCER_NAME}/fetchWeather`)

export const { setWeather } = weatherSlice.actions

export default weatherSlice.reducer;