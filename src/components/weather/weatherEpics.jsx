import { from, interval } from 'rxjs';
import { combineEpics, ofType } from 'redux-observable';
import { switchMap, mergeMap, map, toArray, filter, debounceTime } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { fetchWeather, setWeather } from './reducer';
import { weatherAPIKey } from '../../../config';
import { appendEmoji, responseToWeatherDetails } from './weatherUtils';
import { setLoading } from '../loading/reducer';
import { of } from 'rxjs';

export const fetchWeatherEpic = (action$, state$) =>
    action$.pipe(
        ofType(fetchWeather.type),
        debounceTime(1000),
        switchMap(action =>
            from(action.payload).pipe(
                filter(city => !state$.value.weather.weatherData.has(city.tags.name)),
                mergeMap(city => {
                    const query = `${city.lat},${city.lon}&days=3&aqi=no&alerts=no`;
                    return ajax.getJSON(`http://api.weatherapi.com/v1/forecast.json?key=${weatherAPIKey}&q=${query}`).pipe(
                        map(response => {
                            const weatherDetails = responseToWeatherDetails(response, city);
                            return { city: city.tags.name, weather: weatherDetails };
                        })
                    );
                }),
                toArray(),
                mergeMap(weatherDataArray => {
                    const payloadCityNames = new Set(action.payload.map(city => city.tags.name));
                    const updatedWeatherMap = new Map(
                        [...state$.value.weather.weatherData].filter(([cityName]) =>
                            payloadCityNames.has(cityName)
                        )
                    );

                    weatherDataArray.forEach(item => {
                        updatedWeatherMap.set(item.city, item.weather);
                    });

                    return of(setWeather(updatedWeatherMap), setLoading(false));
                })
            )
        )
    );

const oneHour = 1000 * 60 * 60;
export const refreshWeatherEpic = (action$, state$) =>
    interval(oneHour).pipe(
        map(() => {
            const cities = [...state$.value.weather.weatherData.keys()].map(cityName => {
                const data = state$.value.weather.weatherData.get(cityName);
                return { tags: { name: cityName }, lat: data.lat, lon: data.lon };
            });
            return fetchWeather(cities);
        })
    );

export const weatherEpics = combineEpics(
    fetchWeatherEpic,
    refreshWeatherEpic
)

