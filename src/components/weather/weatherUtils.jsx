export const responseToWeatherDetails = (response, city) => {
    const iconUrl = response?.current?.condition?.icon.split("/");
    const iconName = iconUrl.pop();
    const dayNight = iconUrl.pop();
    const iconPath = `${dayNight}/${iconName}`
    const weatherDetails = {
        temperature: response?.current?.temp_c,
        rain: response?.current?.precip_mm,
        wind: response?.current?.wind_kph,
        icon: iconPath,
        condition: response?.current?.condition?.text,
        pressure: response?.current?.pressure_mb,
        cityLat: city.lat,
        cityLon: city.lon
    };

    return appendEmoji(weatherDetails);
}

export const appendEmoji = (weatherDetails) => {
    var score = 3;
    if (weatherDetails.rain != 0) score -= 1;
    if (weatherDetails.temperature < 18 || weatherDetails.temperature > 25) score -= 1;

    switch (score) {
        case 3:
            weatherDetails.emoji = '😄';
            break;
        case 2:
            weatherDetails.emoji = '😐';
            break;
        case 1:
            weatherDetails.emoji = '🙁';
            break;
    }

    weatherDetails.score = score;

    return weatherDetails;
}
