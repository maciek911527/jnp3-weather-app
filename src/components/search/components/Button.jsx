import styled from "styled-components";

export const Button = styled.button`
    background-color: ${props => props.colorMode === 'dark' ? '#2e2e2d' : '#5898bf'};
    color: ${props => props.colorMode === 'dark' ? '#049104' : 'black'};
    cursor: pointer;
    border: 2px solid ${props => props.colorMode === 'dark' ? 'grey' : 'black'};
    font-weight: bold;
    border-radius: 8px;
    border: 1px solid black;
    padding: 0.6em 1.2em;
    cursor: pointer;
    transition: border-color 0.25s;
    font-family: "Gill Sans", sans-serif;
    font-weight: bold;

    &:hover {
        background-color: #5c5c5b;
    }
`;