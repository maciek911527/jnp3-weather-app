import styled from "styled-components";

export const Filter = styled.span`
  height: 30px;
  width: 30px;
  background-color: ${props => props.enabled ? 'green' : 'black'};
  border-radius: 50%;
  display: inline-block;
  margin-right: 5px;
`;