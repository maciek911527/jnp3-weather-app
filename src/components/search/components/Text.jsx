import styled from "styled-components";

export const Text = styled.div`
    color: ${props => props.colorMode === 'dark' ? '#049104' : 'black'};
    margin-right: 1rem;
`;