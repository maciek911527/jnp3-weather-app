import styled from "styled-components";

export const Input = styled.input`
    padding: 0.5rem;
    margin-right: 1rem;
    border: 1px solid #ddd;
    border-radius: 5px;
    text-align: right;
    height: 1.5vw;
    color: white;
    background-color: ${props => props.colorMode === 'dark' ? '#1d1e1f' : '#365b80'};
`;