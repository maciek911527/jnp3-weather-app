import styled from "styled-components";

export const Tabs = styled.div`
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 10003;
    margin-left: 0px;
`;
