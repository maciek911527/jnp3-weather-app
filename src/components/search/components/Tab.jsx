import styled from "styled-components"

export const Tab = styled.div`
    display: flex;
    align-items: center;
    padding: 0.5rem;
    width: 100%;
    height: 6vh;
    background-color: ${props => props.colorMode === 'dark' ? '#2e2e2d' : '#5898bf'};
    z-index: 10002;
`
