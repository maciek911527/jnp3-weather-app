import React, { useState } from 'react';
import { Tab } from './components/Tab';
import { Tabs } from './components/Tabs';
import { Button } from './components/Button';
import { Input } from './components/Input';
import { Text } from './components/Text';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCities, fetchCitiesWithFilters, getUserLocation, setRecentered } from '../map/reducer';
import { changeChartsEnables, changeColorMode, changeFilterEnabled, setCityInput, setMaxInput, setMinInput } from './reducer';
import { Filter } from './components/Filter';
import { setLoading } from '../loading/reducer';

const Search = () => {
  const dispatch = useDispatch();
  const cityInput = useSelector((state) => state.search.cityInput);
  const minInput = useSelector((state) => state.search.minInput);
  const maxInput = useSelector((state) => state.search.maxInput);
  const colorMode = useSelector((state) => state.search.colorMode);
  const filterEnabled = useSelector((state) => state.search.filterEnabled);
  const chartsEnabled = useSelector((state) => state.search.chartsEnabled);

  const handleRecenter = () => {
    dispatch(setRecentered(false));
    dispatch(getUserLocation());
  };

  const handleSearch = () => {
    if (!filterEnabled) {
      dispatch(changeFilterEnabled());
      dispatch(setLoading(true));
      dispatch(fetchCitiesWithFilters());
    } else {
      dispatch(changeFilterEnabled());
      dispatch(setLoading(true));
      dispatch(fetchCities())
    }
  };

  const handleInputChange = () => {
    if (filterEnabled) {
      dispatch(changeFilterEnabled())
      dispatch(setLoading(true));
      dispatch(fetchCities());
    }
  }

  const handleModeChange = () => {
    dispatch(changeColorMode());
  }

  const handleChartsChange = () => {
    dispatch(changeChartsEnables())
  }

  return (
    <Tabs>
    <Tab colorMode={colorMode}>
      <Text colorMode={colorMode}>Filter cities by population or name using the panel below</Text>
      <Button colorMode={colorMode} style={{ marginLeft: 'auto', marginRight: '2%' }} onClick={handleChartsChange}>{chartsEnabled ? 'Hide charts' : 'Show charts'}</Button> 
      <Button colorMode={colorMode} style={{ marginLeft: '0px', marginRight: '2%' }} onClick={handleModeChange}>{colorMode == "dark" ? 'Change to light mode' : 'Change to dark mode'}</Button> 
    </Tab>
    <Tab colorMode={colorMode}>
      <Text colorMode={colorMode}>City </Text>
      <Input 
        colorMode={colorMode}
        style={{ textAlign: 'center' }}
        type="text" 
        placeholder="City Name"
        value={cityInput}
        onChange={(e) => {dispatch(setCityInput(e.target.value)); handleInputChange();}}
      />
      {cityInput == "" && (
        <>

          <Text colorMode={colorMode}>Min</Text>
          <Input
            colorMode={colorMode}
            type="range"
            min="0"
            max="5000000"
            placeholder="0"
            value={minInput}
            onChange={(e) => {dispatch(setMinInput(e.target.value)); handleInputChange()}}
          />
          <Text colorMode={colorMode}>{minInput}</Text>
          <Text colorMode={colorMode}>Max</Text>
          <Input
            colorMode={colorMode}
            type="range"
            min="0"
            max="5000000"
            placeholder="5000000"
            value={maxInput}
            onChange={(e) => {dispatch(setMaxInput(e.target.value)); handleInputChange();}}
          />
          <Text colorMode={colorMode}>{maxInput}</Text>
        </>
      )}
      <Button colorMode={colorMode} style={{width: '10vw'}} onClick={handleSearch}>{filterEnabled ? 'Disable Filter' : 'Enable Filter'}</Button> 
      <Filter colorMode={colorMode} style={{ marginLeft: '15px' }} enabled={filterEnabled} />
      <Button colorMode={colorMode} style={{ marginLeft: 'auto', marginRight: '2%' }} onClick={handleRecenter}>Recenter</Button> 
    </Tab>
    </Tabs>
  );
};

export default Search;
