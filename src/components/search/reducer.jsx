import { createSlice } from "@reduxjs/toolkit";

export const SEARCH_REDUCER_NAME = 'search';

const searchSlice = createSlice({
  name: SEARCH_REDUCER_NAME,
  initialState: {
    cityInput: "",
    minInput: 1,
    maxInput: 5000000,
    filterEnabled: false,
    chartsEnabled: false,
    colorMode: "dark",
  },
  reducers: {
    setCityInput: (state, action) => {
      state.cityInput = action.payload;
    },
    setMinInput: (state, action) => {
      state.minInput = action.payload;
    },
    setMaxInput: (state, action) => {
      state.maxInput = action.payload;
    },
    changeFilterEnabled: (state) => {
      state.filterEnabled = !state.filterEnabled;
    },
    changeChartsEnables: (state) => {
      state.chartsEnabled = !state.chartsEnabled;
    },
    changeColorMode: (state) => {
      state.colorMode = state.colorMode == "dark" ? "light" : "dark" 
    }
  },
});

export const { setCityInput, setMinInput, setMaxInput, changeFilterEnabled, changeChartsEnables, changeColorMode} = searchSlice.actions;

export default searchSlice.reducer;