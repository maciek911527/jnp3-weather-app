import React from 'react';
import { useSelector, useDispatch, batch } from 'react-redux';
import { MapContainer, TileLayer, Marker, Popup, useMapEvents, useMap } from 'react-leaflet';
import { fetchCities, fetchCitiesWithFilters, getUserLocation, setBounds, setRecentered } from './reducer';
import { setLoading } from '../loading/reducer';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';

const Map = () => {
    const dispatch = useDispatch()
    const weatherData = useSelector((state) => state.weather.weatherData)
    const userLocation = useSelector((state) => state.map.userLocation)
    const recentered = useSelector((state) => state.map.recentered)
    const colorMode = useSelector((state) => state.search.colorMode)
    const filterEnabled = useSelector((state) => state.search.filterEnabled)

    const defaultLocation = [0.00, 0.00]

    if (!recentered) {
        dispatch(getUserLocation())
    }

    const RecenterMap = () => {
        if (!recentered && userLocation) {
            const map = useMap();
            map.setView(userLocation, map.getZoom());
            dispatch(setRecentered(true));
        }

        return null;
    };

    const RefreshCities = () => {
        const map = useMapEvents({
            moveend() {
                handleMoveEnd()
            }
        });

        const handleMoveEnd = () => {
            dispatch(setLoading(true));
            const bounds = map.getBounds();
            dispatch(setBounds([bounds.getSouth(), bounds.getWest(), bounds.getNorth(), bounds.getEast()]))
            if (!filterEnabled) {
                dispatch(fetchCities());
            } else {
                dispatch(fetchCitiesWithFilters())
            }
        }

        return null;
    };

    return (
        <MapContainer
            center={defaultLocation}
            zoom={13}
            scrollWheelZoom={true}
            style={{ minHeight: '100vh', minWidth: '100vw' }}
        >
            <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <RefreshCities />
            <RecenterMap />
            {[...weatherData.entries()].map(([cityName, weatherDetails]) => {
                const customIcon = L.divIcon({
                    html: `<div style="display: flex; align-items: center; background-color: ${colorMode == "dark" ? "#2e2e2d" : "#5898bf"}">
                    <img src="${weatherDetails.icon}" style="width: 50px; height: 50px;"/>
                    <span style="font-size: 25px;">${weatherDetails.emoji}</span>
                 </div>`,
                    iconSize: [80, 50]
                });

                return (
                    <Marker
                        key={cityName}
                        position={[weatherDetails.cityLat, weatherDetails.cityLon]}
                        icon={customIcon}
                    >
                        <Popup>
                            <div>
                                <h3>{cityName}</h3>
                                <img src={weatherDetails.icon} alt={cityName + " weather icon"} />
                                <p>Temperature: {weatherDetails.temperature}°C</p>
                                <p>Pressure: {weatherDetails.pressure} hPa</p>
                                <p>Rain: {weatherDetails.rain} mm</p>
                                <p>Wind: {weatherDetails.wind} kph</p>
                                <p>Condition: {weatherDetails.condition}</p>
                            </div>
                        </Popup>
                    </Marker>
                );
            })}
        </MapContainer>
    );
};

export default Map;
