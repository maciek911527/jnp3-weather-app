import { createAction, createSlice } from "@reduxjs/toolkit";

export const CITIES_REDUCER_NAME = 'map';

const citiesSlice = createSlice({
    name: CITIES_REDUCER_NAME,
    initialState: {
        cities: [],
        userLocation: null,
        recentered: false,
        bounds: []
    },
    reducers: {
        setCities: (state, action) => {
            state.cities = action.payload;
        },
        setUserLocation: (state, action) => {
            state.userLocation = action.payload
        },
        setRecentered: (state, action) => {
            state.recentered = action.payload
        },
        setBounds: (state, action) => {
            state.bounds = action.payload
        }
    },
});

export const fetchCities = createAction(`${CITIES_REDUCER_NAME}/fetchCities`)
export const fetchCitiesWithFilters = createAction(`${CITIES_REDUCER_NAME}/fetchCitiesWithFilters`)
export const getUserLocation = createAction(`${CITIES_REDUCER_NAME}/getUserLocation`)

export const { setCities, setUserLocation, setRecentered, setBounds } = citiesSlice.actions;

export default citiesSlice.reducer;