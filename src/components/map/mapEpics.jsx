import { combineEpics, ofType } from 'redux-observable';
import { debounceTime, mergeMap, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { fetchCities, getUserLocation, setUserLocation, fetchCitiesWithFilters } from './reducer';
import { of } from 'rxjs';
import { fetchWeather, setWeather } from '../weather/reducer';
import { setLoading } from '../loading/reducer';

export const fetchCitiesEpic = (action$, state$) =>
    action$.pipe(
        ofType(fetchCities.type),
        debounceTime(1000),
        switchMap(() => {
            const [south, west, north, east] = state$.value.map.bounds;
            const query = `[out:json];node[place="city"](${south},${west},${north},${east});out;`;

            return ajax.getJSON(`https://overpass-api.de/api/interpreter?data=${encodeURIComponent(query)}`).pipe(
                switchMap((response) => {
                    const sortedCities = response.elements.sort((a, b) => {
                        const populationA = parseInt(a.tags.population, 10) || 0;
                        const populationB = parseInt(b.tags.population, 10) || 0;
                        return populationB - populationA;
                    });

                    const top20Cities = sortedCities.slice(0, 20);
                    return of(fetchWeather(top20Cities));
                })
            );
        })
    );



export const fetchCitiesWithFiltersEpic = (action$, state$) =>
    action$.pipe(
        ofType(fetchCitiesWithFilters.type),
        switchMap(() => {
            const minPopulation = state$.value.search.minInput;
            const maxPopulation = state$.value.search.maxInput;
            const cityName = state$.value.search.cityInput;

            const [south, west, north, east] = state$.value.map.bounds;

            let query = `[out:json];node[place="city"](${south},${west},${north},${east})`;
            if (cityName) {
                query += `[name~"^${cityName}$", i]`;
            }
            query += `;out;`;

            return ajax.getJSON(`https://overpass-api.de/api/interpreter?data=${encodeURIComponent(query)}`).pipe(
                switchMap((response) => {
                    let cities = response.elements;

                    if (cities.length == 0) {
                        return of(setWeather(new Map()), setLoading(false));
                    }
                    else if (cityName) {
                        const city = cities[0];
                        return of(fetchWeather([city]));
                    } else {
                        cities = cities.filter(city => {
                            const population = parseInt(city.tags.population, 10) || 0;
                            return population >= minPopulation && population <= maxPopulation;
                        });

                        cities.sort((a, b) => {
                            const populationA = parseInt(a.tags.population, 10) || 0;
                            const populationB = parseInt(b.tags.population, 10) || 0;
                            return populationB - populationA;
                        });

                        const topCities = cities.slice(0, 20);
                        return of(fetchWeather(topCities));
                    }
                })
            );
        })
    );


export const getUserLocationEpic = (action$) =>
    action$.pipe(
        ofType(getUserLocation.type),
        mergeMap(() => {
            return new Observable(observer => {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(
                        position => {
                            observer.next(setUserLocation([position.coords.latitude, position.coords.longitude]));
                            observer.complete();
                        },
                        error => observer.error(error)
                    );
                }
            });
        })
    );


export const mapEpics = combineEpics(
    fetchCitiesEpic,
    getUserLocationEpic,
    fetchCitiesWithFiltersEpic
)