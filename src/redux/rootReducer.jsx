import { combineReducers } from 'redux';
import citiesReducer from '../components/map/reducer'
import loaderReducer from '../components/loading/reducer'
import weatherReducer from '../components/weather/reducer'
import searchReducer from '../components/search/reducer'

const rootReducer = combineReducers({
    map: citiesReducer,
    loader: loaderReducer,
    weather: weatherReducer,
    search: searchReducer
});

export default rootReducer;
