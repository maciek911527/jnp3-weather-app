import { combineEpics } from "redux-observable"
import { mapEpics } from "../components/map/mapEpics"
import { weatherEpics } from "../components/weather/weatherEpics"

const rootEpic = combineEpics(
    mapEpics,
    weatherEpics
)

export default rootEpic